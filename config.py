# -*- coding: utf-8 -*-
import os
import re
import socket
import subprocess
from libqtile.config import KeyChord, Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import bar, extension, hook, layout, widget
from libqtile.lazy import lazy
from typing import List  # noqa: F401

mod = "mod4"                                     # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"                             # My terminal of choice
myConfig = "/home/lucifer/.config/qtile/config.py"    # The Qtile config file location

keys = [
         ### The essentials
         Key([mod, "shift"], "Return",
             lazy.spawn(myTerm),
             desc='Launches My Terminal'
             ),
         Key([mod], "p",
             lazy.spawn("dmenu_run -c -l 15"),
             desc='Dmenu Run Launcher'
             ),
         Key([mod], "space",
             lazy.next_layout(),
             desc='Toggle through layouts'
             ),
         Key([mod, "shift"], "c",
             lazy.window.kill(),
             desc='Kill active window'
             ),
         Key([mod, "control"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
         Key([mod, "control"], "q",
             lazy.shutdown(),
             desc='Shutdown Qtile'
             ),
         Key(["control", "shift"], "b",
             lazy.spawn(myTerm+" -e bmenu"),
             desc='Doom Emacs'
             ),
         Key([mod], "0",
             lazy.spawn(os.path.expanduser('~') + "/.config/dmenu/dmenu-script-exit.sh"),
             desc= "exit"),
         ### Window controls
         Key([mod], "k",
             lazy.layout.down(),
             desc='Move focus down in current stack pane'
             ),
         Key([mod], "j",
             lazy.layout.up(),
             desc='Move focus up in current stack pane'
             ),
         Key([mod, "shift"], "k",
             lazy.layout.shuffle_down(),
             desc='Move windows down in current stack'
             ),
         Key([mod, "shift"], "j",
             lazy.layout.shuffle_up(),
             desc='Move windows up in current stack'
             ),
         Key([mod], "h",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
         Key([mod], "l",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
         Key([mod, "shift"], "f",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
         Key([mod, "shift"], "m",
             lazy.window.toggle_fullscreen(),
             desc='toggle fullscreen'
             ),
         ### My applications launched with SUPER + Shift  + KEY
         Key([mod, "shift"], "b",
             lazy.spawn(myTerm+" -e bmenu"),
             desc='Doom Emacs'
             ),
         Key([mod, "shift"], "1",
             lazy.spawn(myTerm+" -e ranger"),
             desc='ranger'
             ),
         Key([mod, "shift"], "2",
             lazy.spawn(myTerm+" -e nvim"),
             desc='neo vim'
             ),
         Key([], "Print",
             lazy.spawn("scrot '%Y-%m-%d-@%H-%M-%S-scrot.png' -e 'notify-send -u low -t 3000 \'Scrot\' \'Full Screenshot Saved\''"),
             desc = "Print Whole Screen"
             ),
         Key(["control"], "Print",
             lazy.spawn("scrot -s '%Y-%m-%d-@%H-%M-%S-scrot.png' -e 'notify-send -u low -t 3000 \'Scrot\' \'Screenshot of Section Saved\''"),
             desc="Print screen"
             ),
         ### My applications launched with SUPER + KEY
         Key([mod], "1",
             lazy.spawn("brave"),
             desc='brave browser'
             ),
         Key([mod], "2",
             lazy.spawn("spotify"),
             desc='spotify'
             ),
         Key([mod], "3",
             lazy.spawn("deluge-gtk"),
             desc='deluge torrent client'
             ),
]
#s v u 3 ª r 5 h ´
#ª
group_names = [("q", {'layout': 'monadtall'}),
               ("w", {'layout': 'max'}),
               ("e", {'layout': 'monadtall'}),
               ("r", {'layout': 'floating'})]

groups = [Group(name=name, label="", **kwargs) for name, kwargs in group_names]

for (name, kwargs) in group_names:
    keys.append(Key([mod], str(name), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(name), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {"border_width": 2,
                "margin": 6,
                "border_focus": "ff5555",
                "border_normal": "1D2330"
                }

layouts = [
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    layout.Floating(**layout_theme),
]

colors = [["#000000", "#000000"], # panel background
          ["#434758", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#8d62a9", "#8d62a9"], # border line color for other tab and odd widgets
          ["#668bd7", "#668bd7"], # color for the even widgets
          ["#e1acff", "#e1acff"]] # window name

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Consolas",
    fontsize = 12,
    padding = 2,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.GroupBox(
                       fontsize = 12,
                       margin_y = 3,
                       margin_x = 0,
                       padding_y = 5,
                       padding_x = 3,
                       borderwidth = 0,
                       disable_drag = True,
                       block_highlight_text_color = colors[2],
                       active = colors[6],
                       inactive = colors[1],
                       rounded = True,
                       highlight_method = "border",
                       background = colors[0],
                       ),
              widget.Prompt(
                       prompt = prompt,
                       font = "Ubuntu Mono",
                       padding = 10,
                       foreground = colors[3],
                       background = colors[1]
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 10,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.WindowName(
                       foreground = colors[6],
                       background = colors[0],
                       padding = 0
                       ),
              widget.Net(
                      font = "Consolas",
                      fontsize = 12,
                      background = colors[0],
                      foreground = colors[2],
                      interface = 'wlp2s0',
                      format = '{down} ↓↑{up} | ',
                      padding = 0
                      ),
              widget.Clock(
                       foreground = colors[2],
                       background = colors[0],
                       format = "%b %d, %H:%M | "
                       ),
              widget.BatteryIcon(
                       background = colors[0],
                       padding = 0,
                       update_interval = 1
                      ),
              widget.Battery(
                      background = colors[0],
                      foreground = colors[2],
                      format = " {percent:2.0%} {hour:d}:{min:02d} |",
                      update_interval = 1, 
                      ),
              widget.Systray(
                       background = colors[0],
                       padding = 5
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[0],
                       ),
              widget.CurrentLayoutIcon(
                       foreground = colors[0],
                       background = colors[0],
                       padding = 0,
                       scale = 0.7
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[0],
                       ),
              ]
    return widgets_list

def init_widgets_screen():
    widgets_screen = init_widgets_list()
    return widgets_screen

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen(), opacity=1.0, size=20))]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
